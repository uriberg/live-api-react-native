import React from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import Icon from "react-native-vector-icons/FontAwesome";

const Item = (props) => {
    return (
        <View style={styles.itemWrapper}>
            <View style={styles.itemText}>
                <Text>{props.title}</Text>
                <Text>${(props.price * 1).toFixed(6)}</Text>
                <Text style={{ color: props.change < 0 ? "red" : "green" }}>{(props.change * 100).toFixed(2)}%</Text>
            </View>
            <View>
                <TouchableOpacity onPress={props.toggleFavorite}>
                    <Icon
                        name="heart"
                        size={30}
                        backgroundColor="transparent"
                        color={props.isFavorite ? "red" : "white"}>
                    </Icon>
                </TouchableOpacity>
            </View>
        </View>
    );
}

export default Item;

const styles = StyleSheet.create({
    itemWrapper:{
        flexDirection: "row",
        justifyContent: "space-between",
        paddingHorizontal: 20,
        flex: 1,
        alignItems: 'center',
    },
    itemText: {
        justifyContent: 'center',
        maxWidth: 150,
    },
});