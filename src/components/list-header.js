import React from 'react';
import { View, Text, StyleSheet} from 'react-native';


const ListHeader = (props) => {
    return (
        <View>
            <Text style={styles.header}>{props.title}</Text>
        </View>
    );
}

export default ListHeader;

const styles = StyleSheet.create({
    header: {
        fontSize: 30,
        paddingVertical: 15,
        fontWeight: 'bold',
        textAlign: 'center',
        // backgroundColor: '#DCDCDC',
        backgroundColor: '#BF9A92',
        color: 'white'
    },
});