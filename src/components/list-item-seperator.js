import React from 'react';
import { View, StyleSheet} from 'react-native';


const ListItemSeperator = () => {
    return <View style={styles.itemSeparator}/>;
}

export default ListItemSeperator;

const styles = StyleSheet.create({
    itemSeparator: {
        backgroundColor: 'black',
        height: 1,
    },
});