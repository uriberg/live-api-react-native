import { observable, action, makeObservable} from 'mobx';

class LoginStore {
   isLoggedIn = false;


    constructor(rootStore) {
        makeObservable(this, {
            isLoggedIn: observable,
            setLoginStatus: action,
        });
        this.rootStore = rootStore;
        // autorun(() => console.log(this.report));
    }

    setLoginStatus(loginStatus){
        console.log('in mobx');
        this.isLoggedIn = loginStatus;
    }

}

export default LoginStore;
