import { observable, action, makeObservable, autorun } from 'mobx';
import { CURRENCIES_API_KEY } from '@env';
import axiosInstance from '../axios';

class CurrenciesStore {
    currencies = [];
    popularCurrencies = [];


    constructor(rootStore) {
        makeObservable(this, {
            currencies: observable,
            popularCurrencies: observable,
            setLoginStatus: action,
            setCurrencies: action,
            togglePopularCurrency: action,
            fetchCurrencies: action,
            //disposer: action
        });

        this.fetchCurrencies();
        //this.disposer();
        setInterval(() => {
            this.fetchCurrencies();
        }, 1000)
        this.rootStore = rootStore;
        // autorun(() => {
        //     if(this.currencies){
        //     console.log('in autorun');    
        //     this.fetchCurrencies()
        //     }
        // }, {delay: 3000});
    }



    setLoginStatus(loginStatus) {
        this.isLoggedIn = loginStatus;
    }

    setCurrencies(currencies) {
        //console.log('in mobx currencies');
        this.currencies = currencies;

    }



    fetchC = async () => {
        const result =  await axiosInstance().get('currencies/ticker?key=' + CURRENCIES_API_KEY + '&ids=BTC,ETH,XRP,USDT,BNB,DOT,ADA,LTC,LINK,BCH,USDC,XLM,UNI,WBTC,DOGE,XEM,HEX,OKB,ALGO,ATOM');
        return result;
    }

    // disposer = autorun (() => {
    //     console.log(this.currencies);
    //     if(this.currencies){
    //     this.fetchC().then((res) => {
    //         console.log('in AUTORUN');
    //         if(this.currencies){
    //             console.log(this.currencies);
    //         this.currencies = res.data;
    //         }
    //         //this.setCurrencies(res.data);
    //     })
    // }
    // });

    fetchCurrencies() {
        axiosInstance().get('currencies/ticker?key=' + CURRENCIES_API_KEY + '&ids=BTC,ETH,XRP,USDT,BNB,DOT,ADA,LTC,LINK,BCH,USDC,XLM,UNI,WBTC,DOGE,XEM,HEX,OKB,ALGO,ATOM')
            .then(result => {
                // console.log(result);
                this.setCurrencies(result.data);
            })
            .catch(error => console.log(error));
    }

    addToPopularCurrencies(movieTitle, id) {
        if (this.popularCurrencies.findIndex((element) => element.id === id) === -1) {
            let currPopularCurrencies = [...this.popularCurrencies, { movieTitle, id }];
            this.wishlist = currPopularCurrencies;
        }
    }

    deleteFromPopularCurrencies(id) {
        let deletedIndex = this.popularCurrencies.findIndex((element) => element.id === id);
        if (deletedIndex > -1) {
            let currPopularCurrencies = [...this.popularCurrencies];
            currPopularCurrencies.splice(deletedIndex, 1);
            this.popularCurrencies = currPopularCurrencies;
        }
    }

    togglePopularCurrency(id) {
        let toggledIndex = this.popularCurrencies.findIndex((element) => element.id === id);
        let currPopularCurrencies = [...this.popularCurrencies];
        if (toggledIndex > -1) {
            currPopularCurrencies.splice(toggledIndex, 1);

        }
        else {
            currPopularCurrencies = [...this.popularCurrencies, { id }];
        }
        this.popularCurrencies = currPopularCurrencies;
    }

}

export default CurrenciesStore;
