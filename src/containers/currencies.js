import React, { Component } from 'react';
import { View, FlatList, TouchableHighlight, StyleSheet, Text } from 'react-native';
import { inject, observer } from 'mobx-react';
import Item from '../components/item';
import ListHeader from '../components/list-header';
import ListItemSeperator from '../components/list-item-seperator';


// @inject(stores => ({
//     currenciesStore: stores.rootStore.currenciesStore,
// }))
// @observer
const Currencies = inject(stores => ({ currenciesStore: stores.rootStore.currenciesStore }))(observer((props) => {

    const renderItem = ({ item }) => (
        <TouchableHighlight onPress={() => console.log('pressed')} underlayColor={'#f1f1f1'}>
            <View style={styles.listItem}>
                <Item title={item.name}
                    price={item.price}
                    change={item['1d'].price_change_pct}
                    toggleFavorite={() => props.currenciesStore.togglePopularCurrency(item.id)}
                    isFavorite={props.currenciesStore.popularCurrencies.findIndex((element) => element.id === item.id) > -1}
                    key={item.name} />
            </View>
        </TouchableHighlight>
    );

    const favorite = props.currenciesStore.popularCurrencies;

    return (
        <View>
            <FlatList
                data={props.currenciesStore.currencies}
                renderItem={renderItem}
                keyExtractor={item => item.name}
                ItemSeparatorComponent={ListItemSeperator}
                ListHeaderComponent={() => <ListHeader title={"Currencies"} />}
                contentContainerStyle={{
                    flexGrow: 1,
                }} />
            {/* <Text>Currencies</Text> */}
        </View>
    );
}));

export default Currencies;

// constructor(props) {
//     super(props);
// }

// componentDidMount() {

//     this.props.currenciesStore.fetchCurrencies();

//     this._interval = setInterval(() => {
//        this.props.currenciesStore.fetchCurrencies();
//     }, 3000);
// }


// componentWillUnmount() {
//     clearInterval(this._interval);
// }

// toggleFavorite(id){
//     this.props.currenciesStore.togglePopularCurrency(id);
// }



const styles = StyleSheet.create({
    listContainer: {
        flex: 1,
        paddingVertical: 10,
        paddingHorizontal: 20,
        zIndex: 5,
        backgroundColor: 'white',
    },
    listItem: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingVertical: 10,
        paddingHorizontal: 10,
    },
});
