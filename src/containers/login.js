import React, {Component} from 'react';
import {View, ImageBackground, StyleSheet, Text} from 'react-native';
import {LoginButton, AccessToken} from 'react-native-fbsdk';
import {Button, Headline} from 'react-native-paper';
import {inject, observer} from 'mobx-react';
import bitcoinPic from '../../assets/bitcoin.jpg';

@inject(stores => ({
    loginStore: stores.rootStore.loginStore,
}))
@observer
export default class Login extends Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        console.log(this.props.loginStore.isLoggedIn);
        AccessToken.getCurrentAccessToken().then(
            (data) => {
                if (data !== null){
                    console.log(data.accessToken.toString());
                    this.props.loginStore.setLoginStatus(true);
                }
            });
    }

    render() {

        return (
            <View style={styles.loginContainer}>
                <ImageBackground source={bitcoinPic}
                                 style={styles.image}>
                    <Headline style={styles.headline}>
                        Live Cryptocurrency Prices
                    </Headline>
                    <View style={styles.landingButtons}>
                        <LoginButton
                            onLoginFinished={
                                (error, result) => {
                                    if (error) {
                                        console.log("login has error: " + result.error);
                                    } else if (result.isCancelled) {
                                        console.log("login is cancelled.");
                                    } else {
                                        AccessToken.getCurrentAccessToken().then(
                                            (data) => {
                                                console.log(data.accessToken.toString());
                                                this.props.loginStore.setLoginStatus(true);
                                            }
                                        );
                                    }
                                }
                            }
                            onLogoutFinished={() => {
                                console.log("logout.");
                                this.props.loginStore.setLoginStatus(false);
                            }}/>
                        <View style={styles.currencyButton}>
                            {this.props.loginStore.isLoggedIn ?
                                <Button mode="contained" icon="currency-btc"
                                        onPress={() => this.props.navigation.navigate('Currencies')}>
                                    Go To Currencies
                                </Button>
                            : null}
                        </View>
                    </View>
                </ImageBackground>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    loginContainer: {
        flex: 1
    },
    headline: {
        fontFamily: 'MuseoModerno',
        padding: 10,
        alignSelf: "center",
        color: "white",
        marginTop: 20,
    },
    landingButtons: {
        padding: 20, justifyContent: "center", alignItems: "center", flex: 1
    },
    currencyButton: {
        padding: 20,
    },
    image: {
        flex: 1,
        resizeMode: "cover",
        justifyContent: "center"
    },
});
