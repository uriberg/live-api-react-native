import React, { Component } from 'react';
import { View, TouchableHighlight, FlatList, StyleSheet } from 'react-native';
import { inject, observer } from 'mobx-react';
import Item from '../components/item';
import ListHeader from '../components/list-header';


// @inject(stores => ({
//     currenciesStore: stores.rootStore.currenciesStore,
// }))
// @observer
// export default class Favorites extends Component {
//     constructor(props) {
//         super(props);
//     }
const Favorites = inject(stores => ({ currenciesStore: stores.rootStore.currenciesStore }))(observer((props) => {
    // componentDidMount() {
    //     this.props.currenciesStore.fetchCurrencies();

    //     this._interval = setInterval(() => {
    //        this.props.currenciesStore.fetchCurrencies();
    //     }, 3000);
    // }

    // componentWillUnmount() {
    //     clearInterval(this._interval);
    // }

    // toggleFavorite(id) {
    //     this.props.currenciesStore.togglePopularCurrency(id);
    // }

    const renderItem = ({ item }) => (
        props.currenciesStore.popularCurrencies.findIndex((element) => element.id === item.id) > -1 ?
            <View>
                <TouchableHighlight onPress={() => console.log('pressed')} underlayColor={'#f1f1f1'}>
                    <View style={styles.listItem}>
                        <Item title={item.name}
                            price={item.price}
                            change={item['1d'].price_change_pct}
                            toggleFavorite={() => props.currenciesStore.togglePopularCurrency(item.id)}
                            isFavorite={props.currenciesStore.popularCurrencies.findIndex((element) => element.id === item.id) > -1}
                            key={item.name} />
                    </View>
                </TouchableHighlight>
                <View style={styles.itemSeparator} />
            </View> : null
    );

    return (
        <View>
            <FlatList
                data={props.currenciesStore.currencies}
                renderItem={renderItem}
                keyExtractor={item => item.name}
                // ItemSeparatorComponent={this.renderSeparator}
                ListHeaderComponent={() => <ListHeader title={"Favorite Currencies"} />}
                contentContainerStyle={{
                    flexGrow: 1,
                }} />
        </View>
    );
}));

export default Favorites;

const styles = StyleSheet.create({
    listContainer: {
        flex: 1,
        paddingVertical: 10,
        paddingHorizontal: 20,
        zIndex: 5,
        backgroundColor: 'white',
    },
    listItem: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingVertical: 10,
        paddingHorizontal: 10,
    },
    itemSeparator: {
        backgroundColor: 'black',
        height: 1,
    },
});
