import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import { createDrawerNavigator } from '@react-navigation/drawer';
import Login from "../containers/login";
import Currencies from '../containers/currencies';
import Favorites from "../containers/favorites";
import Landing from '../components/landing';

const Stack = createStackNavigator();

const Drawer = createDrawerNavigator();

const DrawerNavigator = () => {
  return (
    <Drawer.Navigator>
      <Drawer.Screen name="Currencies" component={Currencies} />
      <Drawer.Screen name="Favorites" component={Favorites} />
    </Drawer.Navigator>
  );
};

const MainStackNavigator = () => {
  return (
    <Stack.Navigator initialRouteName="Home">
      <Stack.Screen name="Home" component={Login} />
      <Stack.Screen name="Landing" component={Landing}/>
      <Stack.Screen name="Currencies" component={DrawerNavigator} />
    </Stack.Navigator>
  );
};

export { MainStackNavigator };