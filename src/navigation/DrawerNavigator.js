import React from 'react';
import { createDrawerNavigator } from '@react-navigation/drawer';
import Currencies from '../containers/currencies';
import Favorites from '../containers/favorites';


const Drawer = createDrawerNavigator();

const DrawerNavigator = () => {
  return (
    <Drawer.Navigator>
      <Drawer.Screen name="Home" component={Currencies} />
    </Drawer.Navigator>
  );
};

export default DrawerNavigator;
