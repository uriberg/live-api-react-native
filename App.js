/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { Component } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
// import { createDrawerNavigator } from '@react-navigation/drawer';
import Login from './src/containers/login';
import Currencies from './src/containers/currencies';
import { Provider as PaperProvider } from 'react-native-paper';
import { Provider as StoreProvider } from 'mobx-react';
import LoginStore from './src/store/login-store';
import CurrenciesStore from './src/store/currencies-store';
import { MainStackNavigator } from './src/navigation/StackNavigator';

import branch, { BranchEvent } from 'react-native-branch';
import { navigationRef } from './RootNavigation';
import * as RootNavigation from './RootNavigation';


class RootStore {
  constructor() {
    this.loginStore = new LoginStore(this);
    this.currenciesStore = new CurrenciesStore(this);
  }
}
const rootStore = new RootStore();

  export default class App extends React.Component {
    navigationRef = null;

    constructor(props){
      super(props);
    }

    async componentDidMount() {
      console.log('in mount');
      branch.initSessionTtl = 10000;
      branch.subscribe(({ error, params }) => {
        console.log('in subscriebee!!!!');
        console.log('innnnnnnnnnnnnnnnnn');
        if (error) {
          console.error('Error from Branch: ' + error)
          return
        }
      
        // params will never be null if error is null
      
        if (params['+non_branch_link']) {
          const nonBranchUrl = params['+non_branch_link']
          // Route non-Branch URL if appropriate.
          return
        }
      
        if (!params['+clicked_branch_link']) {
          // Indicates initialization success and some other conditions.
          // No link was opened.
          return
        }
      
        // A Branch link was opened.
        // Route link based on data in params, e.g.
    
        // Now push the view for this URL
        console.log('this is urk', params.$android_deeplink_path);
        if (params.$android_deeplink_path !== undefined){
          RootNavigation.navigate(`${params.$android_deeplink_path}`, {});
         }
      })

      // let lastParams = await branch.getLatestReferringParams();
    }

    render(){
      return (
        <StoreProvider rootStore={rootStore}>
          <PaperProvider>
            <NavigationContainer ref={navigationRef}>
              <MainStackNavigator />
            </NavigationContainer>
          </PaperProvider>
        </StoreProvider>
      );
    }
};

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: Colors.lighter,
  },
  engine: {
    position: 'absolute',
    right: 0,
  },
  body: {
    backgroundColor: Colors.white,
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: Colors.black,
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: Colors.dark,
  },
  highlight: {
    fontWeight: '700',
  },
  footer: {
    color: Colors.dark,
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },
});

// export default App;
